import React, { useState, useContext } from 'react';
import {
    View,
    Text,
    FlatList,
} from 'react-native';

import {RootContext} from './index';

const Soal2 = () => {

    const state = useContext(RootContext);
    
    const renderItem = ({item}) => {
        return(
            <View>
                <Text>{item.name}</Text>
                <Text>{item.position}</Text>
            </View>
        )
    }
    
    console.log(state)
    return(
        <View>            
            <FlatList
                data={state.name}
                renderItem={renderItem}
            />
        </View>
    )
}

export default Soal2;